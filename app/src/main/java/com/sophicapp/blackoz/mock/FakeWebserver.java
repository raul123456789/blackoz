package com.sophicapp.blackoz.mock;

import com.sophicapp.blackoz.Categories.ProductCategoryMenModel;
import com.sophicapp.blackoz.Categories.ProductCategoryModel;
import com.sophicapp.blackoz.model.CenterRepository;

import java.util.ArrayList;

public class FakeWebserver {

    private static FakeWebserver fakeWebserver;

    public static FakeWebserver getFakeWebserver(){

        if(fakeWebserver==null){
            fakeWebserver=new FakeWebserver();
        }
        return fakeWebserver;
    }

    public void addCategory()  {

        ArrayList<ProductCategoryModel> listCategory = new ArrayList<ProductCategoryModel>();

        listCategory.add(new ProductCategoryModel(
                                "Men",
                                "Mens Wear",
                                "https://images.unsplash.com/photo-1552252059-9d77e4059ad1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=332&q=80"

        ));

        listCategory.add(new ProductCategoryModel(
                "Womens",
                "Womens Wear",
                "https://images.unsplash.com/photo-1552664199-fd31f7431a55?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80"
        ));
        CenterRepository.getCenterRepository().setListCategory(listCategory);
    }

    public void addMenCategory() {

        ArrayList<ProductCategoryMenModel> listMenCategory = new ArrayList<ProductCategoryMenModel>();

        listMenCategory.add(new ProductCategoryMenModel(

                    "Mens Shirts",
                    "Shirts",
                    "https://images.unsplash.com/photo-1543084951-1650d1468e2d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"

        ));

        listMenCategory.add(new ProductCategoryMenModel(
                    "Mens Pants",
                    "Pants",
                    "https://images.unsplash.com/photo-1493357335960-4583bfa6f8d9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"

        ));
        CenterRepository.getCenterRepository().setListMenSubCategory(listMenCategory);
    }


    public void getSubCategory(int productCategory) {

        if(productCategory==0) {

            addMenCategory();

        } else{



        }






    }













}
