package com.sophicapp.blackoz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class PaymentModes extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private PaymentCredintials paymentCredintials=new PaymentCredintials();
    private BankCredintials bankCredintials = new BankCredintials();


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PaymentModes() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PaymentModes newInstance(String param1, String param2) {
        PaymentModes fragment = new PaymentModes();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_modes,container,false);
        Button gPay,ppay,paytm,bank;
        bank=(RadioButton)view.findViewById(R.id.banktrnsfr);
        gPay=(Button)view.findViewById(R.id.gpay);
        ppay=(RadioButton)view.findViewById(R.id.ppay);
        paytm=(RadioButton)view.findViewById(R.id.paytm);


        bank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.paymentFrgmnt,bankCredintials);
                transaction.commit();

            }
        });

        paytm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.paymentFrgmnt,paymentCredintials);
                transaction.commit();
            }
        });

        ppay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.paymentFrgmnt,paymentCredintials);
                transaction.commit();
            }
        });


        gPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.paymentFrgmnt,paymentCredintials);
                transaction.commit();
            }
        });
        //description.setText(R.string.manuDesc);
        return view;
    }
}
