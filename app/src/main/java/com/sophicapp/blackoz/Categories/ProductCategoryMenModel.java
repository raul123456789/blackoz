package com.sophicapp.blackoz.Categories;

public class ProductCategoryMenModel {

    private String menSubCategoryName;
    private String menSubCategoryDesc;
    private String imageUrl;


    public ProductCategoryMenModel(String menSubCategoryName, String menSubCategoryDesc, String imageUrl) {
        super();
        this.menSubCategoryName = menSubCategoryName;
        this.menSubCategoryDesc = menSubCategoryDesc;
        this.imageUrl = imageUrl;
    }

    public String getMenSubCategoryName() {
        return menSubCategoryName;
    }

    public void setMenSubCategoryName(String menSubCategoryName) {
        this.menSubCategoryName = menSubCategoryName;
    }

    public String getMenSubCategoryDesc() {
        return menSubCategoryDesc;
    }

    public void setMenSubCategoryDesc(String menSubCategoryDesc) {
        this.menSubCategoryDesc = menSubCategoryDesc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
