package com.sophicapp.blackoz.Categories;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sophicapp.blackoz.Api.ProductCategoryLoaderTask;
import com.sophicapp.blackoz.R;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ProductCategories extends Fragment {

    private RecyclerView recyclerView;

    private boolean doubleBackToExitPressedOnce;

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    private Handler mHandler = new Handler();

    public ProductCategories() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_product_categories, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        new ProductCategoryLoaderTask(
                getActivity(), recyclerView).execute();


       view.setFocusableInTouchMode(true);
       view.requestFocus();
       view.setOnKeyListener(new View.OnKeyListener() {
           @Override
           public boolean onKey(View v, int keyCode, KeyEvent event) {
               if(event.getAction() == KeyEvent
                       .ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {

                   if(doubleBackToExitPressedOnce){

                       if(mHandler!=null){
                           mHandler.removeCallbacks(mRunnable);
                       }

                       getActivity().finish();

                       return true;

                   }
                   doubleBackToExitPressedOnce=true;
                   Toast.makeText(getActivity(),"Please click BACK again to exit"
                           ,Toast.LENGTH_LONG).show();
               }
               return true;
           }
       });

        return view;
    }

}
