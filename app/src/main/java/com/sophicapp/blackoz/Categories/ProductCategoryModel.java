package com.sophicapp.blackoz.Categories;

public class ProductCategoryModel {

        private String categoryName;
        private String categoryDescription;
        private String categoryImageUrl;

    public ProductCategoryModel
            (String categoryName, String categoryDescription,
             String categoryImageUrl) {
        super();
        this.categoryName = categoryName;
        this.categoryDescription = categoryDescription;
        this.categoryImageUrl = categoryImageUrl;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String productCategoryId) {
        this.categoryName = productCategoryId;
    }

    public String getCategoryDescription() {
        return categoryDescription;
    }

    public void setCategoryDescription(String categoryDescription) {
        this.categoryDescription = categoryDescription;
    }

    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }

    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }
}
