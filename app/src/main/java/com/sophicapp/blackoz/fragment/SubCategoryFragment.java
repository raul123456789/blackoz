package com.sophicapp.blackoz.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sophicapp.blackoz.Api.ProductCategoryMenLoaderTask;
import com.sophicapp.blackoz.R;
import com.sophicapp.blackoz.mock.FakeWebserver;
import com.sophicapp.blackoz.util.AppConstants;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SubCategoryFragment extends Fragment {

    private RecyclerView recyclerView;

    private boolean doubleBackToExitPressedOnce;

    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            doubleBackToExitPressedOnce = false;
        }
    };

    private Handler mHandler = new Handler();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_sub_category, container, false);

        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerView2);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        new ProductCategoryMenLoaderTask(
                getActivity(), recyclerView).execute();
        FakeWebserver.getFakeWebserver().
                        getSubCategory(AppConstants.CURRENT_CATEGORY);

        return view;
    }

    public SubCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
