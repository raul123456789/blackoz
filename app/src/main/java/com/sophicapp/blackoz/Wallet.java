package com.sophicapp.blackoz;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.sophicapp.blackoz.ui.gallery.GalleryFragment;
import com.sophicapp.blackoz.ui.slideshow.SlideshowFragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

public class Wallet extends AppCompatActivity {

    ImageView imageView;
    Button trnsfr;
    GalleryFragment galleryFragment=new GalleryFragment();
    SlideshowFragment slideshowFragment = new SlideshowFragment();
    Transactions transactFragment = new Transactions();
    PaymentModes paymentModes = new PaymentModes();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        imageView=(ImageView)findViewById(R.id.logo);
        trnsfr=(Button)findViewById(R.id.transfer);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentgl, transactFragment);
        // Commit the transaction
        transaction.commit();

        trnsfr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction1 = getSupportFragmentManager().beginTransaction();
                transaction1.replace(R.id.fragmentgl, paymentModes);
                transaction1.addToBackStack(null);
                transaction1.commit();

            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().show();
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.side_nav_bar);

    }
}
