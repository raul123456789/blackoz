package com.sophicapp.blackoz;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.sophicapp.blackoz.Categories.ProductCategories;
import com.sophicapp.blackoz.ui.gallery.GalleryFragment;
import com.sophicapp.blackoz.ui.home.HomeFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    FirebaseAuth fAuth;
    TextView headerEmail;
    Button home,btnwallet,manufctr,mClock,productCat;
    ExpandableListView expandableListView;
    GalleryFragment galleryFragment=new GalleryFragment();
    AboutUs aboutUs=new AboutUs();
    MClock mclock=new MClock();
    Manufacturers manufacturers=new Manufacturers();
    PrivacyPolicy privacyPolicy=new PrivacyPolicy();
    HomeFragment homeFragment=new HomeFragment();
    Orders orderFrgment = new Orders();
    Cart cartFragment = new Cart();
    Account myAccount=new Account();
    Contactus contactus=new Contactus();
    Compaint compaint=new Compaint();
    Refer referFrgment=new Refer();
    ProductCategories productCategoriesFragment=new ProductCategories();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        btnwallet=(Button)findViewById(R.id.button2);
        mClock=(Button)findViewById(R.id.mclock);
        expandableListView=(ExpandableListView)findViewById(R.id.item23);
        home=(Button)findViewById(R.id.button1);
        manufctr=(Button)findViewById(R.id.manufctr);
        productCat=(Button)findViewById(R.id.categories);

        displayHomePage();



        productCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment3,productCategoriesFragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });


        mClock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment3,mclock);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment3,homeFragment);
                transaction.commit();

            }
        });

        manufctr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment3,manufacturers);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        btnwallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fAuth=FirebaseAuth.getInstance();
                FirebaseUser user =fAuth.getCurrentUser();
                if(user!=null) {
                    startActivity(new Intent(MainActivity.this,Wallet.class));
                } else if(user == null) {
                    startActivity(new Intent(MainActivity.this,Login.class));
                }
            }
        });

        setSupportActionBar(toolbar);
        getSupportActionBar().show();
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setLogo(R.drawable.blckoz3);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.side_nav_bar);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        headerEmail=(TextView)headerView.findViewById(R.id.emailHead);
        mAppBarConfiguration = new AppBarConfiguration.Builder (
                R.id.nav_home,
                R.id.nav_gallery,
                R.id.nav_slideshow,
                R.id.nav_mCLockwallet,
                R.id.nav_refer,
                R.id.nav_faq,
                R.id.nav_about,
                R.id.nav_contact,
                R.id.nav_complaint,
                R.id.nav_terms,
                R.id.nav_login,R.id.nav_logout)
                .setDrawerLayout(drawerLayout)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.nav_home:
                        Intent intent = new Intent(MainActivity.this, MainActivity.class);
                        startActivity(intent);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.fragment3,homeFragment);
                        transaction.commit();
                        return true;

                    case R.id.nav_Order:
                        drawerLayout.closeDrawers();
                        FragmentTransaction tran1 = getSupportFragmentManager().beginTransaction();
                        tran1.replace(R.id.fragment3,orderFrgment);
                        tran1.commit();
                        return true;

                    case R.id.nav_slideshow:
                        drawerLayout.closeDrawers();
                        FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
                        transaction2.replace(R.id.fragment3,cartFragment);
                        transaction2.addToBackStack(null);
                        transaction2.commit();
                        return true;

                    case R.id.nav_contact:
                        drawerLayout.closeDrawers();
                        FragmentTransaction trans2 = getSupportFragmentManager().beginTransaction();
                        trans2.replace(R.id.fragment3,contactus);
                        trans2.addToBackStack(null);
                        trans2.commit();
                        return true;

                    case R.id.nav_complaint:
                        drawerLayout.closeDrawers();
                        FragmentTransaction trans5 = getSupportFragmentManager().beginTransaction();
                        trans5.replace(R.id.fragment3,compaint);
                        trans5.addToBackStack(null);
                        trans5.commit();
                        return true;

                    case R.id.nav_myAccount:

                        drawerLayout.closeDrawers();
                        FragmentTransaction transaction3 = getSupportFragmentManager().beginTransaction();
                        transaction3.replace(R.id.fragment3,myAccount);
                        transaction3.addToBackStack(null);
                        transaction3.commit();
                        return true;

                    case R.id.nav_refer:

                        drawerLayout.closeDrawers();
                        FragmentTransaction refer = getSupportFragmentManager().beginTransaction();
                        refer.replace(R.id.fragment3,referFrgment);
                        refer.addToBackStack(null);
                        refer.commit();
                        return true;

                    case R.id.nav_gallery:

                        drawerLayout.closeDrawers();
                        FragmentTransaction frgmnt = getSupportFragmentManager().beginTransaction();
                        frgmnt.replace(R.id.fragment3,orderFrgment);
                        frgmnt.addToBackStack(null);
                        frgmnt.commit();

                    case R.id.nav_login:

                        Intent intent1 = new Intent(MainActivity.this, Login.class);
                        startActivity(intent1);
                        return true;

                    case R.id.nav_wallet_amount:

                        Intent in2 = new Intent(MainActivity.this, Wallet.class);
                        startActivity(in2);
                        return true;

                    case R.id.nav_cashback_Amount:

                        Intent in = new Intent(MainActivity.this, CashbackAmount.class);
                        startActivity(in);
                        return true;

                    case R.id.nav_mCLockwallet:

                        Intent in3 = new Intent(MainActivity.this, MClockWallet.class);
                        startActivity(in3);
                        return true;

                    case R.id.nav_logout:

                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                        finish();
                        return true;

                    case R.id.nav_about:

                        drawerLayout.closeDrawers();
                        FragmentTransaction frgmnt1 = getSupportFragmentManager().beginTransaction();
                        frgmnt1.replace(R.id.fragment3,aboutUs);
                        frgmnt1.addToBackStack(null);
                        frgmnt1.commit();
                        return true;

                    case R.id.nav_terms:

                        drawerLayout.closeDrawers();
                        FragmentTransaction frgmnt2 = getSupportFragmentManager().beginTransaction();
                        frgmnt2.replace(R.id.fragment3,privacyPolicy);
                        frgmnt2.addToBackStack(null);
                        frgmnt2.commit();
                        return true;
                    }
                    return true;
                }
            }
        );
        displayUserDetails();

    }

    private void displayHomePage() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment3,homeFragment);
        transaction.commit();
    }

    private void displayUserDetails() {

        fAuth=FirebaseAuth.getInstance();
        FirebaseUser user =fAuth.getCurrentUser();
        if(user!=null) {
            hideItem();
            if(headerEmail!=null){
                headerEmail.setText("\n\n Welcome \n "+user.getEmail());
            }

        }else if(user == null) {
            show();
            if(headerEmail!=null) {
                headerEmail.setText("     Not Logged In");
            }
        }
    }


    private void hideItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_login).setVisible(false);
        nav_Menu.findItem(R.id.nav_wallet_amount).setVisible(true);
        nav_Menu.findItem(R.id.nav_cashback_Amount).setVisible(true);
        nav_Menu.findItem(R.id.nav_myAccount).setVisible(true);
    }
    private void show() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_logout).setVisible(false);
        nav_Menu.findItem(R.id.nav_login).setVisible(true);
        nav_Menu.findItem(R.id.nav_wallet_amount).setVisible(false);
        nav_Menu.findItem(R.id.nav_cashback_Amount).setVisible(false);
        nav_Menu.findItem(R.id.nav_myAccount).setVisible(false);
        nav_Menu.findItem(R.id.nav_mCLockwallet).setVisible(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


}
