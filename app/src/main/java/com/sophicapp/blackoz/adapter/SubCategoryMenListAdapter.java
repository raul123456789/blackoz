package com.sophicapp.blackoz.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sophicapp.blackoz.Categories.ProductCategoryMenModel;
import com.sophicapp.blackoz.R;
import com.sophicapp.blackoz.model.CenterRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SubCategoryMenListAdapter extends RecyclerView.Adapter<SubCategoryMenListAdapter.VersionViewHolder> {

        public static List<ProductCategoryMenModel> categoryMenModelList = new ArrayList<ProductCategoryMenModel>();
        private Context context;
        OnItemClickListener onItemClickListener;

        //private TextDrawable drawable;
        private String productImageUrl;


        public SubCategoryMenListAdapter(Context context) {
                categoryMenModelList= CenterRepository.getCenterRepository().getListMenSubCategory();
                this.context = context;
        }

        @NonNull
        @Override
        public VersionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view= LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_men_subcategory_list,parent,false);
                VersionViewHolder viewHolder=new VersionViewHolder(view);
                return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull VersionViewHolder holder, int position) {

                holder.itemName.setText(categoryMenModelList.get(position).getMenSubCategoryName());

                holder.itemDesc.setText(categoryMenModelList.get(position).getMenSubCategoryDesc());

                productImageUrl=categoryMenModelList.get(position).getImageUrl();

                Glide.with(context).load(productImageUrl)
                        .centerCrop().into(holder.imageView);
        }

        @Override
        public int getItemCount() {
                return categoryMenModelList==null?0:categoryMenModelList.size();
        }


        public interface OnItemClickListener {
                void onItemClick(View view, int position);
        }

        public void SetOnItemClickListener(OnItemClickListener onItemClickListener) {
                this.onItemClickListener= onItemClickListener;

        }


        class VersionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

                TextView itemName,itemDesc;
                ImageView imageView;

                public VersionViewHolder(@NonNull View itemView) {
                        super(itemView);
                        itemName=(TextView)itemView.findViewById(R.id.item_sub_name);
                        itemDesc=(TextView)itemView.findViewById(R.id.item_sub_desc);
                        itemName.setSelected(true);
                        imageView=(ImageView)itemView.findViewById(R.id.imageViewSub);

                        itemView.setOnClickListener(this);

                }

                @Override
                public void onClick(View v) {
                        onItemClickListener.onItemClick(v,getPosition());
                }
        }






}
