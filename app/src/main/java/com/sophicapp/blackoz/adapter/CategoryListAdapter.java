package com.sophicapp.blackoz.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sophicapp.blackoz.Categories.ProductCategoryModel;
import com.sophicapp.blackoz.R;
import com.sophicapp.blackoz.model.CenterRepository;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.VersionViewHolder>  {

    public static List<ProductCategoryModel> categoryModelList = new ArrayList<ProductCategoryModel>();
    private Context context;
    OnItemClickListener clickListener;
    //private TextDrawable drawable;
    private String productImageUrl;

    public CategoryListAdapter(Context context) {
        categoryModelList= CenterRepository.getCenterRepository().getListCategory();
        this.context = context;
    }

    @NonNull
    @Override
    public VersionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_category_list,parent,false);
        VersionViewHolder viewHolder=new VersionViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VersionViewHolder versionviewholder, int categoryIndex) {

        versionviewholder.itemName.setText(categoryModelList.get(categoryIndex).getCategoryName());

        versionviewholder.itemDesc.setText(categoryModelList.get(categoryIndex).getCategoryDescription());

        productImageUrl=categoryModelList.get(categoryIndex).getCategoryImageUrl();

        Glide.with(context).load(productImageUrl)
                .centerCrop().into(versionviewholder.imageView);

    }

    @Override
    public int getItemCount() {
        return categoryModelList==null?0:categoryModelList.size();
    }

    public void SetOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.clickListener= onItemClickListener;

    }

    public interface OnItemClickListener {
                 void onItemClick(View view, int position);
    }


    class VersionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView itemName,itemDesc;
        ImageView imageView;

        public VersionViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName = (TextView) itemView.findViewById(R.id.item_name);

            itemDesc = (TextView) itemView.findViewById(R.id.item_short_desc);

            itemName.setSelected(true);

            imageView = ((ImageView) itemView.findViewById(R.id.imageView));

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onItemClick(v,getPosition());
        }
    }
}




