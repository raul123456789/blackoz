package com.sophicapp.blackoz;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

public class EditAccount extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EditAccount() {
        // Required empty public constructor
    }

    public static EditAccount newInstance(String param1, String param2) {
        EditAccount fragment = new EditAccount();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_edit_account, container, false);

        final EditText textName,textEmail,textPhone,address,city;
        textName=(EditText)view.findViewById(R.id.editName);
        textEmail=(EditText)view.findViewById(R.id.editEmail);
        textPhone=(EditText)view.findViewById(R.id.editPhone);
        address=(EditText)view.findViewById(R.id.editAddress);
        city=(EditText)view.findViewById(R.id.editCity);
        Button updateDetails=(Button)view.findViewById(R.id.updateAcntInfo);
        updateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences preferences1 = getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences1.edit();
                editor.putString("FullName",textName.getText().toString());
                editor.putString("Phone",textPhone.getText().toString());
                editor.putString("Address",address.getText().toString());
                editor.putString("City",city.getText().toString());
                editor.commit();
                Toast.makeText(getActivity(),"Profile Updated",Toast.LENGTH_LONG).show();
                replaceFragment();

            }
        });



        FirebaseAuth fAuth= FirebaseAuth.getInstance();
        FirebaseUser user =fAuth.getCurrentUser();
        SharedPreferences preferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String name = preferences.getString("FullName", "");
        String editPhone=preferences.getString("Phone","");
        String address1=preferences.getString("Address","");
        String city1=preferences.getString("City","");
        textName.setText(name);
        textEmail.setText(user.getEmail());
        textPhone.setText(editPhone);
        address.setText(address1);
        city.setText(city1);
        return view;
    }


    private void replaceFragment() {
        final Account accountFrgmnt=new Account();
        FragmentTransaction transaction2 = getActivity().getSupportFragmentManager().beginTransaction();
        transaction2.replace(R.id.fragment3,accountFrgmnt);
        transaction2.addToBackStack(null);
        transaction2.commit();
    }


}
