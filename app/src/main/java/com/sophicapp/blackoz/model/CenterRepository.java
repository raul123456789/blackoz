package com.sophicapp.blackoz.model;
import com.sophicapp.blackoz.Categories.ProductCategoryMenModel;
import com.sophicapp.blackoz.Categories.ProductCategoryModel;

import java.util.ArrayList;

public class CenterRepository {

    private static CenterRepository centerRepository;
    private ArrayList<ProductCategoryModel> listCategory=new ArrayList<ProductCategoryModel>();
    private ArrayList<ProductCategoryMenModel> listMenSubCategory=new ArrayList<ProductCategoryMenModel>();

    public static CenterRepository getCenterRepository() {

        if(centerRepository == null){
            centerRepository=new CenterRepository();
        }
        return centerRepository;
    }

    public ArrayList<ProductCategoryMenModel> getListMenSubCategory() {
        return listMenSubCategory;
    }

    public void setListMenSubCategory(ArrayList<ProductCategoryMenModel> listMenSubCategory) {
        this.listMenSubCategory = listMenSubCategory;
    }

    public ArrayList<ProductCategoryModel> getListCategory() {
        return listCategory;
    }

    public void setListCategory(ArrayList<ProductCategoryModel> listCategory) {
        this.listCategory = listCategory;
    }


}
