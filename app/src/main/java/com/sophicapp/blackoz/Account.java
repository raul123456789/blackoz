package com.sophicapp.blackoz;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


public class Account extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TextView userEmail,userCountry,userPhone,userName,userAddress,userCity;
    private EditAccount editAccount=new EditAccount();

    private String mParam1;
    private String mParam2;

    public Account() {

    }

    public static Account newInstance(String param1, String param2) {
        Account fragment = new Account();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_account,container,false);
        Button edit=(Button)view.findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction2 = getActivity().getSupportFragmentManager().beginTransaction();
                transaction2.replace(R.id.fragment3,editAccount);
                transaction2.addToBackStack(null);
                transaction2.commit();

            }
        });

        displayUserDetails(view);
        return view;
    }

    private void displayUserDetails(View view) {

        SharedPreferences preferences = this.getActivity().getSharedPreferences("pref", Context.MODE_PRIVATE);
        String name = preferences.getString("FullName", "");
        String phone=preferences.getString("Phone","");
        String address=preferences.getString("Address","");
        String city=preferences.getString("City","");
        userEmail=(TextView)view.findViewById(R.id.userEmail);
        userCountry=(TextView)view.findViewById(R.id.userCountry);
        userName=(TextView)view.findViewById(R.id.userName);
        userPhone=(TextView)view.findViewById(R.id.userPhone);
        userAddress=(TextView)view.findViewById(R.id.addressAcnt);
        userCity=(TextView)view.findViewById(R.id.cityAcnt);
        FirebaseAuth fAuth= FirebaseAuth.getInstance();
        FirebaseUser user =fAuth.getCurrentUser();
        if(user!=null) {
            if(userEmail!=null) {
                userEmail.setText("              "+user.getEmail());
                userName.setText("   "+name);
                userPhone.setText("   "+phone);
                userCountry.setText("   India");
                userAddress.setText(" "+address);
                userCity.setText(" "+city);
            }
        } else if(user == null) {

        }



    }
}
