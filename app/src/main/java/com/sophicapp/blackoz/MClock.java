package com.sophicapp.blackoz;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MClock#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MClock extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private MMClock mmClock=new MMClock();
    private RechargeMClock rechargeMClock=new RechargeMClock();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public MClock() {
        // Required empty public constructor
    }

    public static MClock newInstance(String param1, String param2) {
        MClock fragment = new MClock();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_m_clock,container,false);
        Button rechrge=(Button)view.findViewById(R.id.pnts);
        rechrge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction frgmnt2 = getActivity().getSupportFragmentManager().beginTransaction();
                frgmnt2.replace(R.id.fragment3,rechargeMClock);
                frgmnt2.addToBackStack(null);
                frgmnt2.commit();

            }
        });
        TextView mclck=(TextView)view.findViewById(R.id.mclck);
        mclck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction frgmnt2 = getActivity().getSupportFragmentManager().beginTransaction();
                frgmnt2.replace(R.id.fragment3,mmClock);
                frgmnt2.addToBackStack(null);
                frgmnt2.commit();
            }
        });

        return view;
    }
}
