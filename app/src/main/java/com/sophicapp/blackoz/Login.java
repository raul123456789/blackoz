package com.sophicapp.blackoz;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class Login extends AppCompatActivity {

    EditText nEmail,nPassword;
    TextView forgtpasswd,signup;
    Button login,close;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nEmail=(EditText)findViewById(R.id.email);
        navigationView=(NavigationView)findViewById(R.id.nav_view);
        nPassword=(EditText)findViewById(R.id.password);
        fAuth=FirebaseAuth.getInstance();
        forgtpasswd=(TextView)findViewById(R.id.frgtpasswd);
        signup=(TextView)findViewById(R.id.signup);
        login=(Button)findViewById(R.id.login2);
        progressBar=(ProgressBar)findViewById(R.id.progressBar2);

        forgtpasswd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final EditText resetMail= new EditText(v.getContext());
                AlertDialog.Builder passwordResetDialog=new AlertDialog.Builder(v.getContext());
                passwordResetDialog.setTitle("Reset Password");
                passwordResetDialog.setMessage("Enter Email to Receive Reset Password Link");
                passwordResetDialog.setView(resetMail);

                passwordResetDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String mail= resetMail.getText().toString();
                        fAuth.sendPasswordResetEmail(mail).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                Toast.makeText(Login.this,"Reset Link sent to Your Email",Toast.LENGTH_LONG).show();

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                                Toast.makeText(Login.this,"Error ! Reset Link is Not Sent "+e.getMessage(),Toast.LENGTH_LONG).show();

                            }
                        });


                    }
                });

                passwordResetDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                passwordResetDialog.create().show();

            }
        });


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String femail= nEmail.getText().toString();
                String fpassword=nPassword.getText().toString();

                if(TextUtils.isEmpty(femail)) {
                   nEmail.setError("Email is Required");
                   return;
                }
                if(TextUtils.isEmpty(fpassword)) {
                   nPassword.setError("Password is Required");
                   return;
                }

                if(fpassword.length() < 6) {
                    nPassword.setError("Password must be atleast 8 characters");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                fAuth.signInWithEmailAndPassword(femail,fpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                     if(task.isSuccessful()) {
                         Toast.makeText(Login.this,"Logged In Successfully",Toast.LENGTH_LONG).show();
                         Intent intent = new Intent(Login.this, MainActivity.class);
                         startActivity(intent);
                     }  else {
                            Toast.makeText(Login.this,"Error :"+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this,SignUp.class);
                startActivity(intent);

            }
        });

    }

    private void hide() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_login).setVisible(false);
    }
}
