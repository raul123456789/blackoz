package com.sophicapp.blackoz.Api;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.sophicapp.blackoz.adapter.SubCategoryMenListAdapter;
import com.sophicapp.blackoz.mock.FakeWebserver;

import androidx.recyclerview.widget.RecyclerView;

public class ProductCategoryMenLoaderTask extends AsyncTask<String,Void,Void> {

    private static final int NUMBER_OF_COLUMNS=2;
    private Context context;
    private RecyclerView recyclerView;

    public ProductCategoryMenLoaderTask(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... strings) {

        try {
            Thread.sleep(300);
        }catch(Exception e) {
            e.printStackTrace();
        }

       FakeWebserver.getFakeWebserver().addMenCategory();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(recyclerView!=null) {

            SubCategoryMenListAdapter subCategoryMenListAdapter=new SubCategoryMenListAdapter(context);

            recyclerView.setAdapter(subCategoryMenListAdapter);

            subCategoryMenListAdapter.SetOnItemClickListener(new SubCategoryMenListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {



                }
            });
        }
    }
}
