package com.sophicapp.blackoz.Api;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import com.sophicapp.blackoz.MainActivity;
import com.sophicapp.blackoz.R;
import com.sophicapp.blackoz.adapter.CategoryListAdapter;
import com.sophicapp.blackoz.fragment.SubCategoryFragment;
import com.sophicapp.blackoz.mock.FakeWebserver;
import com.sophicapp.blackoz.util.AppConstants;
import com.sophicapp.blackoz.util.Utils;

import androidx.recyclerview.widget.RecyclerView;

public class ProductCategoryLoaderTask extends AsyncTask<String,Void,Void> {

    private static final int NUMBER_OF_COLUMNS=2;
    private Context context;
    private RecyclerView recyclerView;

    public ProductCategoryLoaderTask(Context context, RecyclerView recyclerView) {
        this.context = context;
        this.recyclerView = recyclerView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... params) {

        try{
            Thread.sleep(300);
        }catch(Exception e) {
            e.printStackTrace();
        }

        FakeWebserver.getFakeWebserver().addCategory();

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if(recyclerView!=null)  {

            CategoryListAdapter categoryListAdapter= new CategoryListAdapter(context);

            recyclerView.setAdapter(categoryListAdapter);

            categoryListAdapter.SetOnItemClickListener(new CategoryListAdapter.OnItemClickListener() {

                @Override
                public void onItemClick(View view, int position) {

                    AppConstants.CURRENT_CATEGORY = position;

                    //Toast.makeText(context,"Clicked", LENGTH_SHORT).show();
                    Utils.switchFragmentWithAnimation (
                            R.id.fragment3,
                            new SubCategoryFragment(),
                            (MainActivity)context, null,
                            Utils.AnimationType.SLIDE_LEFT
                    );
                }
            });



        }


    }
}
