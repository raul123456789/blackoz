package com.sophicapp.blackoz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class SignUp extends AppCompatActivity {

    EditText fullname,nEmail,nPhone,nPassword;
    TextView refcode,login;
    Button regstr;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        login=(TextView)findViewById(R.id.lgn);
        regstr=(Button)findViewById(R.id.rgstr);
        fullname=(EditText)findViewById(R.id.name);
        nEmail=(EditText)findViewById(R.id.email1);
        nPhone=(EditText)findViewById(R.id.phone);
        nPassword=(EditText)findViewById(R.id.passwd);
        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        fAuth=FirebaseAuth.getInstance();
        preferences= getSharedPreferences("pref", Context.MODE_PRIVATE);



        regstr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fname= fullname.getText().toString();
                String femail= nEmail.getText().toString();
                String fpassword=nPassword.getText().toString();
                String phone=nPhone.getText().toString();

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("FullName",fname);
                editor.putString("Phone",phone);
                editor.commit();

                if(TextUtils.isEmpty(femail)) {
                    nEmail.setError("Email is Required");
                    return;
                }

                if(TextUtils.isEmpty(fpassword)) {
                    nPassword.setError("Password is Required");
                    return;
                }
                if(TextUtils.isEmpty(fname)) {
                    fullname.setError("Name is Required");
                    return;
                }

                if(fpassword.length()<6) {
                    nPassword.setError("Password must be atleast 8 characters");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                //register the user
                fAuth.createUserWithEmailAndPassword(femail,fpassword).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful()) {

                            Toast.makeText(SignUp.this,"User Created Successfully",Toast.LENGTH_LONG).show();
                            FirebaseUser user =fAuth.getCurrentUser();
                            user.sendEmailVerification();
                            startActivity(new Intent(getApplicationContext(),Login.class));

                        } else {
                            Toast.makeText(SignUp.this,"Error :"+task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                        }

                    }
                });
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignUp.this,Login.class);
                startActivity(intent);
            }
        });


    }
}
